extern crate iron_testing_example;
extern crate iron;
extern crate reqwest;

use iron_testing_example::start_server;
use iron::Listening;
use reqwest::Client;
use std::io::Read;

#[test]
fn it_works() {
    let server = TestServer::new();
    let client = Client::new().unwrap();

    let url = format!("{}/?str={}&len={}", server.url(), "foo", 5);
    let mut response = client.get(&url).send().unwrap();
    let mut s = String::new();
    response.read_to_string(&mut s).unwrap();

    assert_eq!("  foo", s);
}

struct TestServer(Listening);

impl TestServer {
    fn new() -> TestServer {
        TestServer(start_server("127.0.0.1", 0))
    }

    fn url(&self) -> String {
        format!("http://{}:{}", self.0.socket.ip(), self.0.socket.port())
    }
}

impl Drop for TestServer {
    fn drop(&mut self) {
        self.0.close().expect("Error closing server");
    }
}
